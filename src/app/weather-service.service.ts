import { Injectable } from '@angular/core';

@Injectable()
export class WeatherServiceService {
  private appId = 'a68255d1f5dcfdf3672a181a0347a178';

  getWeatherData(zip: string, count: number){
    return fetch(`http://api.openweathermap.org/data/2.5/forecast/daily?cnt=${count}&zip=${zip},us&appId=${this.appId}`);
  }

 constructor() { }

}
