import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'friendyDayConverter'
})
export class FriendyDayConverterPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    let days = [ 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun' ];
    return days[(new Date(value * 1000)).getDay()];

  }

}
