import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeilComponent } from './neil.component';

describe('NeilComponent', () => {
  let component: NeilComponent;
  let fixture: ComponentFixture<NeilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
