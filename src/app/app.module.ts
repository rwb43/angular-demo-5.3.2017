import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { FourOhFourComponent } from './pages/four-oh-four/four-oh-four.component';
import {RouterModule, Routes} from "@angular/router";
import { NeilComponent } from './pages/neil/neil.component';
import { FiveDayForecastComponent } from './components/five-day-forecast/five-day-forecast.component';
import {WeatherServiceService} from "./weather-service.service";
import { FriendyDayConverterPipe } from './friendy-day-converter.pipe';
import { KelvinToFahrenheitPipe } from './kelvin-to-fahrenheit.pipe';



const routes: Routes = [
  {path: 'home', component: HomePageComponent, children: [
    {path: 'neil', component: NeilComponent}
    ]},
  {path: 'about', component: AboutPageComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '***', component: FourOhFourComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AboutPageComponent,
    FourOhFourComponent,
    NeilComponent,
    FiveDayForecastComponent,
    FriendyDayConverterPipe,
    KelvinToFahrenheitPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [WeatherServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
